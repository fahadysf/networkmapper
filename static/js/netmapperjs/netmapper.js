/**
 * Core JS Functionality for Network Mapper
 */

function stringtodashedUUID(uuid_str) {
    return (uuid_str.substring(0,8)+'-'
    +uuid_str.substring(8,12)+'-'
    +uuid_str.substring(12,16)+'-'
    +uuid_str.substring(16,20)+'-'
    +uuid_str.substring(20,32));
}

function showConnections(connections){
    for (var i=0;i<connections.length;++i){
        connections[i].setVisible(true);
        connections[i].sourcePort.setVisible(true);
        connections[i].targetPort.setVisible(true);
        relocatePorts(connections[i]);
    }
}


function checkLineIntersection(line1StartX, line1StartY, line1EndX, line1EndY,
                               line2StartX, line2StartY, line2EndX, line2EndY) {
    // if the lines intersect, the result contains the x and y of the intersection (treating the lines as
    // infinite) and booleans for whether line segment 1 or line segment 2 contain the point
    var denominator, a, b, numerator1, numerator2, result = {
        x: null,
        y: null,
        onLine1: false,
        onLine2: false
    };
    denominator = ((line2EndY - line2StartY) * (line1EndX - line1StartX)) - ((line2EndX - line2StartX) * (line1EndY - line1StartY));
    if (denominator == 0) {
        return result;
    }
    a = line1StartY - line2StartY;
    b = line1StartX - line2StartX;
    numerator1 = ((line2EndX - line2StartX) * a) - ((line2EndY - line2StartY) * b);
    numerator2 = ((line1EndX - line1StartX) * a) - ((line1EndY - line1StartY) * b);
    a = numerator1 / denominator;
    b = numerator2 / denominator;

    // if we cast these lines infinitely in both directions, they intersect here:
    result.x = line1StartX + (a * (line1EndX - line1StartX));
    result.y = line1StartY + (a * (line1EndY - line1StartY));

    // if line1 is a segment and line2 is infinite, they intersect if:
    if (a > 0 && a < 1) {
        result.onLine1 = true;
    }
    // if line2 is a segment and line1 is infinite, they intersect if:
    if (b > 0 && b < 1) {
        result.onLine2 = true;
    }
    // if line1 and line2 are segments, they intersect if both of the above are true
    return result;
};

function relocatePorts(c) {
    x1 = c.sourcePort.getAbsoluteX();
    x2 = c.targetPort.getAbsoluteX();
    y1 = c.sourcePort.getAbsoluteY();
    y2 = c.targetPort.getAbsoluteY();

    sourcebbox =  c.sourcePort.getParent().getBoundingBox();
    sourceminx = sourcebbox.x;
    sourceminy = sourcebbox.y;
    sourcemaxx = sourcebbox.x+(sourcebbox.w);
    sourcemaxy = sourcebbox.y+(sourcebbox.h);
    var positionfound = false;
    //Test for collision at Source Top boundary
    result = checkLineIntersection(x1,y1,x2,y2,sourceminx,sourceminy, sourcemaxx,sourceminy);
    if ((result.onLine1) && (result.onLine2)){
            positionfound = true;
            c.sourcePort.setX(result.x-sourcebbox.x);
            c.sourcePort.setY(result.y-sourcebbox.y);
        }
    if (positionfound==false) {
        result = checkLineIntersection(x1,y1,x2,y2,sourceminx,sourceminy, sourceminx,sourcemaxy);
        if ((result.onLine1) && (result.onLine2)){
            positionfound = true;
            c.sourcePort.setX(result.x-sourcebbox.x);
            c.sourcePort.setY(result.y-sourcebbox.y);
        }
    }
    if (positionfound==false) {
        result = checkLineIntersection(x1,y1,x2,y2,sourceminx,sourcemaxy, sourcemaxx,sourcemaxy);
        if ((result.onLine1) && (result.onLine2)){
            positionfound = true;
            c.sourcePort.setX(result.x-sourcebbox.x);
            c.sourcePort.setY(result.y-sourcebbox.y);
        }
    }
    if (positionfound==false) {
        result = checkLineIntersection(x1,y1,x2,y2,sourcemaxx,sourceminy, sourcemaxx,sourcemaxy);
        if ((result.onLine1) && (result.onLine2)){
            positionfound = true;
            c.sourcePort.setX(result.x-sourcebbox.x);
            c.sourcePort.setY(result.y-sourcebbox.y);
        }
    }
    targetbbox =  c.targetPort.getParent().getBoundingBox();
    targetminx = targetbbox.x;
    targetminy = targetbbox.y;
    targetmaxx = targetbbox.x+(targetbbox.w);
    targetmaxy = targetbbox.y+(targetbbox.h);
    var positionfound = false;
    //Test for collision at target Top boundary
    result = checkLineIntersection(x1,y1,x2,y2,targetminx,targetminy, targetmaxx,targetminy);
    if ((result.onLine1) && (result.onLine2)){
            positionfound = true;
            c.targetPort.setX(result.x-targetbbox.x);
            c.targetPort.setY(result.y-targetbbox.y);
        }
    if (positionfound==false) {
        result = checkLineIntersection(x1,y1,x2,y2,targetminx,targetminy, targetminx,targetmaxy);
        if ((result.onLine1) && (result.onLine2)){
            positionfound = true;
            c.targetPort.setX(result.x-targetbbox.x);
            c.targetPort.setY(result.y-targetbbox.y);
        }
    }
    if (positionfound==false) {
        result = checkLineIntersection(x1,y1,x2,y2,targetminx,targetmaxy, targetmaxx,targetmaxy);
        if ((result.onLine1) && (result.onLine2)){
            positionfound = true;
            c.targetPort.setX(result.x-targetbbox.x);
            c.targetPort.setY(result.y-targetbbox.y);
        }
    }
    if (positionfound==false) {
        result = checkLineIntersection(x1,y1,x2,y2,targetmaxx,targetminy, targetmaxx,targetmaxy);
        if ((result.onLine1) && (result.onLine2)){
            positionfound = true;
            c.targetPort.setX(result.x-targetbbox.x);
            c.targetPort.setY(result.y-targetbbox.y);
        }
    }
    //Do the same for destination ports
}

function relayerElements(deviceShapes, networkShapes) {
    var devkeys = Object.keys(deviceShapes);
    var netkeys = Object.keys(networkShapes);
    for (var i=0;i<devkeys.length;++i){
        deviceShapes[devkeys[i]]["object_composite"].toFront();
        deviceShapes[devkeys[i]]["bounding_box"].toFront();
        deviceShapes[devkeys[i]]["base_rect"].toBack();
    }
    for (var i=0;i<netkeys.length;++i){
        networkShapes[netkeys[i]]["object_composite"].toFront();
        networkShapes[netkeys[i]]["bounding_box"].toFront();
        networkShapes[netkeys[i]]["base_rect"].toBack();
    }
}

function createShape(obj, xpos, ypos) {
    shapes = {};
    // Create a composite with the id=device.id (UUID)
    var object_composite = new draw2d.shape.composite.Group({
        id: obj.pk,
        x: xpos + 120,
        y: ypos + 76,
        width: 112,
        height: 72,
        cssClass: "draw2d_shape_composite_Group",
        stroke: 1,
        alpha: 0,
        isSelectable: false,
        isDraggable: false
    });
    var base_rect = new draw2d.shape.basic.Rectangle({
        width:110,
        height:70,
        x: xpos+120,
        y: ypos+76,
        keepAspectRatio:true,
        cssClass: "draw2d_shape_basic_Rectangle",
        stroke: 1,
        alpha: 1,
        radius: 10,
        color: "#000000",
        bgColor:  "#ffffff",
        userData: {},
        objectType: "device",
        isDraggable: false,
        ports: []
    });
    var bounding_box = new draw2d.shape.basic.Rectangle({
        x: xpos+120,
        y: ypos+76,
        width: 110,
        height: 70,
        cssClass: "draw2d_shape_basic_Rectangle",
        stroke: 1,
        alpha: 0,
        userData: obj,
        isDraggable: false
    });
    var label = new draw2d.shape.basic.Text({
        x: xpos + 124,
        y: ypos + 113,
        height: 19,
        alpha: 1,
        userData: {},
        color: "#1B1B1B",
        stroke: 0,
        outlineStroke: 0,
        fontSize: 12,
        fontColor: "#000000",
        fontFamily: "Arial",
        isBold: false,
        isFront: true,
        isDraggable: false
    });
    image = new draw2d.shape.basic.Image({
        x: xpos+152,
        y: ypos+80,
        width: 32,
        height: 32,
        userData: {},
        stroke: 1,
        alpha: 1,
        isDraggable: false,
        isSelectable: false
    });
    if (obj.model == "netmapcore.device") {
        label["text"] = obj.fields.hostname;
        image["path"] = static_url+'img/icons/router-96.png';
        base_rect.setStroke(2);
        if (obj.fields.devstatus == "dscv") {
            base_rect.setColor("#ff9933");
        }
        else if (obj.fields.devstatus == "mngd") {
            base_rect.setColor("#33cc33")
        }
        bounding_box.onMouseEnter = function() {
            $("#object-info").html("<div>Entered "+this.userData.fields.hostname+"<br />ID: "+this.userData.pk+"<br /></div>");
        };
        bounding_box.onMouseLeave = function () {
            $("#object-info").html("<div>Left "+this.userData.fields.hostname+"<br />ID: "+this.userData.pk+"<br /></div>");
            $('#secondary-info').html("");
        };
    }
    else if (obj.model == "netmapcore.ipaddress") {
        label["text"] = obj.fields.address+"/"+obj.fields.prefixlen;
        image["path"] = static_url+'img/icons/cloud-512.png';
        bounding_box.onMouseEnter = function() {
            $("#object-info").html("<div>Entered "+this.userData.fields.address+"/"+this.userData.fields.prefixlen
                +"<br />ID: "+this.userData.pk+"<br /></div>");
        };
        bounding_box.onMouseLeave = function () {
            $("#object-info").html("<div>Left "+this.userData.fields.address+"/"+this.userData.fields.prefixlen
                +"<br />ID: "+this.userData.pk+"<br /></div>");
            $('#secondary-info').html("");

        };
    }
    shapes["object_composite"] = object_composite;
    shapes["base_rect"] = base_rect;
    shapes["bounding_box"] = bounding_box;
    shapes["label"] = label;
    shapes["image"] = image;
    return shapes;
}

function setupPort(obj) {
    locator = new draw2d.layout.locator.CenterLocator();
    var port = obj.createPort("hybrid", locator);
    port.setWidth(8); port.setHeight(8); port.setAlpha(1);
    port.setRotationAngle(0);
    port.setStroke(2);
    port.setMaxFanOut(1);
    port.setBackgroundColor("#4F6870");
    port.setVisible(false);
    return port;
}

//Assign interfaces to the device Objects
function setupObjectPorts(canvas, device_objs, interface_objs, network_objs, device_shapes, network_shapes) {
    var connections = Array();
    // Setup ports for devices
    for (var i = 0; i < device_objs.length; i++) {
        var dev = device_objs[i];
        devshape = device_shapes[dev.pk];
        for (var j = 0; j < (interface_objs[dev.pk]).length; j++) {
            var interface = interface_objs[dev.pk][j];
            var port = setupPort(devshape.base_rect);
            port.toBack();
            port.setName(interface.fields.name);
            port.setUserData(interface);
            port.setDraggable(false);
            var netobj = network_objs[stringtodashedUUID(interface.fields.subnet)];
            var network_shape = network_shapes[stringtodashedUUID(interface.fields.subnet)];
            var netport = setupPort(network_shape.base_rect);
            netport.setUserData(netobj);
            netport.toBack();
            port.attr('otherend', netport);
            c = new draw2d.Connection({
                stroke: 3,
                router: new draw2d.layout.connection.SplineConnectionRouter( ),
                source: port,
                target: netport,
                onMouseEnter: function () {
                    this.normalColor = this.getColor();
                    this.setColor(new draw2d.util.Color("#2FA80D"));
                    $("#secondary-info").html(this.sourcePort.name+" connected to "+this.targetPort.userData.fields.address+"/"+this.targetPort.userData.fields.prefixlen);
                },
                onMouseLeave: function (c) {
                    this.setColor(this.normalColor);
                    $("#secondary-info").html("");
                }
            });
            c.setVisible(false);
            connections.push(c);
            canvas.add(c);
        }
    }
    relayerElements(device_shapes, network_shapes);
    return connections;
}

//Function to set up the canvas with Device, and Network Objects
function setupCanvas(canvas, objects, globalpos) {
    // Draw Rectangles for devices
    var shapes = {};
    var i = 0;
    for(i; i<objects.length; ++i) {
        obj = objects[i];
        var objid = obj.pk;
        var xpos = globalpos["x"]*200;
        var ypos = globalpos["y"]*200;
        globalpos["x"] += 1;
        if (globalpos["x"]*200 > canvas.getWidth()-200) {
            globalpos["y"] +=1;
            globalpos["x"] = 0;
        }
        shapes[objid] = createShape(obj, xpos, ypos);
        canvas.add(shapes[objid]["object_composite"]);
        canvas.add(shapes[objid]["base_rect"]);
        canvas.add(shapes[objid]["bounding_box"]);
        canvas.add(shapes[objid]["label"]);
        canvas.add(shapes[objid]["image"]);
        shapes[objid]["object_composite"].assignFigure(shapes[objid]["base_rect"]);
        shapes[objid]["object_composite"].assignFigure(shapes[objid]["bounding_box"]);
        shapes[objid]["object_composite"].assignFigure(shapes[objid]["label"]);
        shapes[objid]["object_composite"].assignFigure(shapes[objid]["image"]);
        comp_fig = shapes[objid]["object_composite"];
    }
    return shapes;
}



