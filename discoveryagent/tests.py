import pprint

from django.test import TestCase
# Create your tests here.
from cliparsers import fortiosdiscovery, ciscoiosdiscovery
from cliparsers import  fortiostasks, ciscoiostasks
from cliparsers import  fortiosparsers, ciscoiosparsers
from netmapcore.models import Credential

dev_cred = Credential.objects.get(mgtip__address='10.255.255.15')
env = {
        'host': dev_cred.mgtip.address,
        'username': dev_cred.username,
        'password': dev_cred.password,
        'proto': dev_cred.proto,
        'port': dev_cred.port,
        'ignorecache': False
    }

print env


class GetInterfaces(TestCase):
    def fortios_test_get_interfaces(self):
        """
        Test to check extraction of interface information
        :return: 0 if successful and 1 if failed
        """
        print("\n-------Starting Interface Info Extraction Test--------\n")
        output = fortiostasks.fortios_get_interfaces(env)
        result = fortiosparsers.parse_interface_config(
            output[0], output[1]
        )
        if 'port1' in result:
            import pprint
            pp = pprint.PrettyPrinter(indent = 2)
            pp.pprint(result)
            return 0
        else:
            return 1
    def ciscoios_test_get_interfaces(self):
        """
        Test to check extraction of interface information
        :return: 0 if successful and 1 if failed
        """
        print("\n-------Starting Interface Info Extraction Test--------\n")
        output = ciscoiostasks.ciscoios_get_interfaces(env)
        result = ciscoiosparsers.parse_interface_config(output)
        if 'port1' in result:
            import pprint
            pp = pprint.PrettyPrinter(indent = 2)
            pp.pprint(result)
            return 0
        else:
            return 1
    def disabled_test_get_interfaces(self):
        return self.ciscoios_test_get_interfaces()

class GetHostname(TestCase):
    def fortios_test_get_hostname(self):
        """
        Test to check extraction of hostname from config system global
        :return: 0 if successful and 1 if failed
        """
        print("\n-------Starting Hostname Extraction Test--------\n")
        output = fortiostasks.fortios_get_config_sys_global(env)
        if fortiosparsers.get_hostname(output) == 'fortigate-vm-1':
            print("Hostname correctly found: %s" % fortiosparsers.get_hostname(output))
            print("\n-------Hostname Extraction Test Successful--------\n")
            return 0
        else:
            print("Hostname does not match")
            print("Hostname found: %s" % fortiosparsers.get_hostname(output))
            return 1
    def ciscoios_test_get_hostname(self):
        """
        Test to check extraction of hostname from config system global
        :return: 0 if successful and 1 if failed
        """
        print("\n-------Starting Hostname Extraction Test--------\n")
        output = ciscoiostasks.ciscoios_get_config(env)
        if ciscoiosparsers.get_hostname(output) == 'R1':
            print("Hostname correctly found: %s" % ciscoiosparsers.get_hostname(output))
            print("\n-------Hostname Extraction Test Successful--------\n")
            return 0
        else:
            print("Hostname does not match")
            print("Hostname found: %s" % fortiosparsers.get_hostname(output))
            return 1
    def disabled_test_get_hostname(self):
        return self.ciscoios_test_get_hostname()

class GetRoutes(TestCase):
    def fortios_test_get_rotues(self):
        """
        Test to get routes on a FortiOS device
        :return: 0 if successful and 1 if failed
        """
        print("\n-------Starting Route Extraction Test--------\n")
        output = fortiostasks.fortios_get_routes(env)
        result = fortiosparsers.parse_routes(output)
        return 0
    def ciscoios_test_get_rotues(self):
        """
        Test to get routes on a FortiOS device
        :return: 0 if successful and 1 if failed
        """
        print("\n-------Starting Route Extraction Test--------\n")
        output = ciscoiostasks.ciscoios_show_ip_route(env)
        result = ciscoiosparsers.parse_routes(output)
        pp = pprint.PrettyPrinter(indent = 2)
        pp.pprint(result)
        return 0
    def disabled_test_get_routes(self):
        return self.ciscoios_test_get_rotues()

class GetMacAddress(TestCase):
    def dc_test_get_mac_addr(self):
        """
        Test to get MAC Address of a fortiOS interface
        :return: 0 if successful and 1 if failed
        """
        print("\n-------Starting MAC Address Test--------\n")
        output = fortiostasks.fortios_get_mac_address("port1", env)
        print output
        return 0


class discoverFortiGate(TestCase):
    def test_discoverDevice(self):
        """

        :return:
        """
        fortiosdiscovery.discoverDevice(ip=env['host'], password='testpass')

class discoverCisco(TestCase):

    def dc_test_discoverDevice(self):
        """

        :return:
        """
        ciscoiosdiscovery.discoverDevice(ip=env['host'], password='testpass')