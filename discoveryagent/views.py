from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from netmapcore.models import Device


def known_devices(request):
    devices = Device.objects.all()
    htmlresponse = "<html><body>"
    num = 0
    for device in devices:
        num+=1
        htmlresponse += "device %d: <br />--------<br />" % num
        htmlresponse += "hostname: %s <br />" % device.hostname
        htmlresponse += "ip address: %s <br />" % device.mgt_ip
    htmlresponse += "</body></html>"
    return HttpResponse(htmlresponse)