from __future__ import unicode_literals

from django.db import models
from django.utils import timezone


# Create your models here.
class CommandCache(models.Model):
    device = models.ForeignKey('netmapcore.Device', db_index=True, related_name='cmdcacheobj_device')
    command = models.CharField(max_length=2048, blank=False)
    tstamp = models.DateTimeField(default=timezone.now, db_index=True)
    output = models.TextField(blank=True)