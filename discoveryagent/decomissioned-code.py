from django.test import TestCase

# Create your tests here.
import cliparsers.fortiostasks as fortiosfabtasks
import cliparsers.fortiosparsers as fortiosparsers
from fabric.tasks import execute
from netmapcore.models import Credential

dev_cred = Credential.objects.first()

fortiosfabtasks.env.passwords = {'%s@%s:%s' % (dev_cred.username, dev_cred.mgt_ip.ipstr(), dev_cred.port) : '%s' % dev_cred.password }
fortiosfabtasks.env.hosts = ['%s@%s:%s' % (dev_cred.username, dev_cred.mgt_ip.ipstr(), dev_cred.port)]




class GetInterfaces(TestCase):
    def test_get_interfaces(self):
        """
        Test to check extraction of interface information
        :return: 0 if successful and 1 if failed
        """
        print("\n-------Starting Interface Info Extraction Test--------\n")
        output = execute(fortiosfabtasks.fortios_get_interfaces)
        result = fortiosparsers.parse_interface_config(
                output[fortiosfabtasks.env.hosts[0]][0],
                output[fortiosfabtasks.env.hosts[0]][1]
        )
        if 'port1' in result:
            import pprint
            pp = pprint.PrettyPrinter(indent = 2)
            pp.pprint(result)
            return 0
        else:
            return 1

class GetHostname(TestCase):
    def test_get_hostname(self):
        """
        Test to check extraction of hostname from config system global
        :return: 0 if successful and 1 if failed
        """
        print("\n-------Starting Hostname Extraction Test--------\n")

        output = execute(fortiosfabtasks.fortios_get_config_sys_global)[fortiosfabtasks.env.hosts[0]]
        if fortiosparsers.get_hostname(output) == 'fortigate-vm-1':
            print("Hostname correctly found: %s" % fortiosparsers.get_hostname(output))
            print("\n-------Hostname Extraction Test Successful--------\n")
            return 0
        else:
            print("Hostname does not match")
            print("Hostname found: %s" % fortiosparsers.get_hostname(output))
            return 1


class GetRoutes(TestCase):
    def test_get_rotues(self):
        """
        Test to get routes on a FortiOS device
        :return: 0 if successful and 1 if failed
        """
        print("\n-------Starting Route Extraction Test--------\n")
        output = execute(fortiosfabtasks.fortios_get_routes)[fortiosfabtasks.env.hosts[0]]
        result = fortiosparsers.parse_routes(output)
        return 0

class GetMacAddress(TestCase):
    def test_get_mac_addr(self):
        """
        Test to get MAC Address of a fortiOS interface
        :return: 0 if successful and 1 if failed
        """
        print("\n-------Starting MAC Address Test--------\n")
        output = execute(fortiosfabtasks.fortios_get_mac_address, "port1")[fortiosfabtasks.env.hosts[0]]
        print output
        return 0