"""
This module aims to create and update model objects for netmapcore from parsed information
extracted through the fortiosparsers module.

Tested against FortiOS versions: 5.2.4
"""
import re

import fortiosparsers
import fortiostasks
from discoveryagent.cliparsers.genericdiscovery import GenericPostProcessor
from netmapcore.models import *

def discoverDevice(ip='0.0.0.0', username='admin', password='testpass', proto='SSH2', port=22):
    """
    :param ip:
    :param username:
    :param password:
    :param proto:
    :param port:
    :return:
    """

    #Set up Tasks
    fortiostasks.env['host'] = ip
    fortiostasks.env['username'] = username
    fortiostasks.env['password'] = password
    fortiostasks.env['port'] = port


    if ip=='0.0.0.0':
        print('Invalid IP')
        return 1

    # Find dievice which already exists with same mgt_ip
    try:
        print("Testing for device with mgt-ip: %s" % ip)
        devobj, new = Device.objects.get_or_create(mgtip__address=ip)
        if new:
            print("Device Object created.")
        else:
            print("Existing device Object found.")
        # Get or create MGT IP IPAddress entry
        ipobj = IPAddress.objects.get_or_create(address=ip, prefixlen=32)[0]
        ipobj.save()
        devobj.mgtip = ipobj
    except:
        print devobj
        raise

    try:
        # Find the Hostname
        output = fortiostasks.fortios_get_config_sys_global(fortiostasks.env)
        devobj.hostname = fortiosparsers.get_hostname(output)
        print ("Hostname Detected: %s" % devobj.hostname)
        devobj.devstatus = 'mngd'
        devobj.save()

        # Get the Interface List
        output = fortiostasks.fortios_get_interfaces(fortiostasks.env)
        interface_dict = fortiosparsers.parse_interface_config(
                output[0],
                output[1]
        )

        # Detect MAC Addresses for each interface
        for intname in interface_dict.keys():
            macaddr = None
            output = fortiostasks.fortios_get_mac_address(intname, fortiostasks.env)
            match = re.search(r'[0-9a-fA-F:]{17}', output)
            if match != None:
                macaddr = match.group()
            interface_dict[intname]['macaddr'] = macaddr

        # Update or create interface objects and directly connected networks+IP Addresses
        for intname in interface_dict.keys():
            if interface_dict[intname].has_key('ipv4'):
                ipobj = IPAddress.objects.get_or_create(address=interface_dict[intname]['ipv4'], prefixlen=32)[0]
                intobj = Interface.objects.get_or_create(device=devobj,
                                                         primary_ip=ipobj)[0]
                network = ipaddress.ip_network(unicode(interface_dict[intname]['ipv4']+'/'+interface_dict[intname]['netmask']), strict=False)
                subnetobj = NetworkSubnet.objects.get_or_create(address=unicode(network.network_address), prefixlen=network.prefixlen)[0]
                subnetobj.connected_to_known_device = True
                macaddrout = fortiostasks.fortios_get_mac_address(intname, fortiostasks.env)
                re_match = re.match(r'.*([A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}).*', macaddrout.replace('\n',' '))
                if re_match == None:
                    intobj.macaddr = '-'
                else:
                    intobj.macaddr = re_match.groups()[0]
                print("Saving interface object %s" % intobj)
                intobj.name = intname
                intobj.primary_ip = ipobj
                intobj.subnet = subnetobj
                ipobj.save()
                subnetobj.save()
                intobj.save()

        # Detect Routes
        output = fortiostasks.fortios_get_routes(fortiostasks.env)
        route_dict = fortiosparsers.parse_routes(output)
        for interface in route_dict.keys():
            for route in route_dict[interface]:
                # Ignore default routes
                if route[1] != '0.0.0.0/0':
                    type = route[0]
                    subnet = route[1]
                    nexthop = route[2]
                    # Create NetworkSubnet if it doesn't exist
                    network = ipaddress.ip_network(unicode(subnet))
                    subnetobj = NetworkSubnet.objects.get_or_create(address=unicode(network.network_address), prefixlen=network.prefixlen)[0]
                    if type == 'Directly Connected':
                        subnetobj.connected_to_known_device = True
                    else:
                        subnetobj.connected_to_known_device = False
                    subnetobj.save()
                    # Create IPObj for nexthop if it doesn't exist
                    routeobj = Route.objects.get_or_create(network=subnetobj, device=devobj)[0]
                    routeobj.subnet = subnetobj
                    routeobj.type = type
                    routeobj.device = devobj
                    routeobj.interface = Interface.objects.get(device=devobj, name=interface)
                    if nexthop != None:
                        nexthop_ipobj = IPAddress.objects.get_or_create(address=nexthop, prefixlen=32)[0]
                        nexthop_ipobj.save()
                        routeobj.nexthop = nexthop_ipobj
                    routeobj.save()

        # initiate post-processing tasks:
        g = GenericPostProcessor()
        orphanroutes = g.get_orphan_nexthops(devobj)
        g.create_discovered_devices(orphanroutes)
        g.cleanup_discovered_devices(devobj)


        """
        if settings.DEBUG:
            import pprint
            pp = pprint.PrettyPrinter(indent = 2)
            pp.pprint(interface_dict)
        """
    except:
        print fortiostasks.env['host']
        print ("Discovery process failed. Make sure you are having the correct device type assigned to the " +
               "Credential Object.")
        print("---Traceback---")
        raise
    # Return the device object created/updated
    return devobj