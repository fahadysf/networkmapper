"""
This package parses the output of various Fortinet FortiOS CLI commands to
extract interface configuration data, routing table information and in the future
also policy information.
"""

import re

def parse_config(config_string):
    """
    Placeholder function to check number of lines in config
    :param config_string:
    :return:
    """
    config_lines = config_string.splitlines()
    print "Number of lines in config: %d" % len(config_lines)

def get_hostname(sys_global_conf):
    sys_global_conf_lines = sys_global_conf.splitlines()
    hostname = "[UNKNOWN]"
    for line in sys_global_conf_lines:
        line = line.strip()
        if line.startswith('set hostname "'):
            hostname = line.split('"')[1]
    return hostname

def parse_interface_config(get_int_output, show_int_output):
    """

    :param get_int_output: Expects output string of 'get system interface'
    :param show_int_output: Expects output string of 'show system interface'
    :return:    Dict of dictionaries containing interface information
                format:
                {
                'port1':
                    {'ip': '192.168.10.10', 'netmask': '255.255.255.0' ... },
                'port2':
                    {'ip': '192.168.20.10', 'netmask': '255.255.255.0' ... },
                ...
                }
    """
    int_block_re = re.compile(r'\s*?edit \"(?P<int_name>\S*)\"\s(?P<int_config>[\s\S]*?)next')
    interfaces = dict()
    interface_paras = re.findall(r'config system interface([\s\S]*)end', show_int_output)[0]
    interface_para_iterator = int_block_re.finditer(interface_paras)
    for para in interface_para_iterator:
        paradict = para.groupdict()
        for i in paradict.keys():
            if i == 'int_name':
                interfaces[paradict[i]] = dict()
                int_name = paradict[i]
            elif i == 'int_config':
                ipdata = re.search(r'set ip (?P<ipv4>[\d\.]*) (?P<netmask>[\d\.]*)',paradict['int_config'])
                if ipdata:
                    ipdatadict = ipdata.groupdict()
                    interfaces[int_name]['ipv4'] = ipdatadict['ipv4']
                    interfaces[int_name]['netmask'] = ipdatadict['netmask']
            else:
                print i
    return interfaces

def parse_routes(get_router_info_output):
    """

    :param get_router_info_output: String containing output of "get router info routing-table all"
    :return: List of routes as a dictionary in format:
    {"interfacename": [ (type, subnet, next-hop), (...) ] }
    """
    route_block_re = re.compile(r'Codes:[\s\S]+candidate default\s*(\S[\S\s]+)$')
    route_block = route_block_re.search(get_router_info_output).group(0)
    route_lines = route_block.splitlines()
    i = 0
    """
    route_dict has format {"interfacename": [(type, subnet, nexthop)]}
    """
    route_dict = dict()
    route_types = {'S': 'Static', 'O': 'OSPF', 'C': 'Directly Connected', 'i': 'IS-IS'}
    for line in route_lines[:-1]:
        if line.strip() != '' and not line.startswith("Codes") and not line.startswith(" "):
            i += 1
            interface = line.split(', ')[1]
            type = route_types[line[0]]
            nexthop = None
            if line[0] == 'C':
                d = re.match(r'^C\s*(?P<subnet>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/\d{1,2})', line).groupdict()
                subnet = d['subnet']
            elif 'via' in line:
                d = re.match(r'^\S*\s\S*?\s+(?P<subnet>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/\d{1,2}) \[\d*/\d*\] via (?P<nexthop>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})',line).groupdict()
                subnet = d['subnet']
                nexthop = d['nexthop']
            if not route_dict.has_key(interface):
                route_dict[interface] = list()
            (route_dict[interface]).append((type, subnet, nexthop))
    import pprint
    pp = pprint.PrettyPrinter(indent = 2)
    pp.pprint(route_dict)
    return route_dict
