"""
This package parses the output of various Cisco IOS CLI commands to
extract interface configuration data, routing table information and in the future
also ACL information.
"""

import pprint
import re

import ipaddress


def parse_config(config_string):
    """
    Placeholder function to check number of lines in config
    :param config_string:
    :return:
    """
    config_lines = config_string.splitlines()
    print "Number of lines in config: %d" % len(config_lines)

def get_hostname(running_config):
    sys_global_conf_lines = running_config.splitlines()
    hostname = "[UNKNOWN]"
    for line in sys_global_conf_lines:
        line = line.strip()
        if line.startswith('hostname '):
            hostname = line.split(' ')[1]
    return hostname

def parse_interface_config(show_interfaces_output):
    """

    :param get_int_output: Expects output string of 'get system interface'
    :param show_int_output: Expects output string of 'show system interface'
    :return:    Dict of dictionaries containing interface information
                format:
                {
                'port1':
                    {'ip': '192.168.10.10', 'netmask': '255.255.255.0' ... },
                'port2':
                    {'ip': '192.168.20.10', 'netmask': '255.255.255.0' ... },
                ...
                }
    """
    paras = []
    index = -1
    cnt = 0
    outlines = show_interfaces_output.splitlines()
    for line in outlines:
        cnt += 1
        if "line protocol" in line:
            index += 1
            paras.append(''.join(line)+'\n')
        elif line.strip()=='':
            pass
        else:
            paras[index] += line+'\n'
    interfaces = dict()
    for para in paras:
        lines = para.splitlines()
        int_name = lines[0].split(' ')[0]
        ipcidr = ''
        for line in lines:
            if line.strip().startswith('Hardware is'):
                macaddr=re.search(r'([a-f0-9]{4}\.[a-f0-9]{4}\.[a-f0-9]{4})', line.strip())
                if macaddr != None:
                    macaddr = macaddr.groups()[0]
                    a = macaddr.split('.')
                    for i,hexet in enumerate(a):
                        a[i] = hexet[0:2]+':'+hexet[2:4]
                    macaddr = ':'.join(a)
                else:
                    macaddr = '-'
            if line.strip().startswith('Internet address is'):
                ipcidr=re.search(r'Internet address is (\S*)', line.strip()).groups()[0]
        if ipcidr != '':
            ipnetobj = ipaddress.ip_network(unicode(ipcidr), strict=False)
            interfaces[int_name] = {'ipv4': ipcidr.split('/')[0], 'netmask': str(ipnetobj.netmask), 'macaddr': macaddr}
    import pprint
    pp = pprint.PrettyPrinter(indent = 2)
    pp.pprint(interfaces)
    return interfaces

def parse_routes(show_ip_route_output):
    """

    :param get_router_info_output: String containing output of "get router info routing-table all"
    :return: List of routes as a dictionary in format:
    {"interfacename": [ (type, subnet, next-hop), (...) ] }
    """

    olines = show_ip_route_output.strip().replace('\r\n', '\n').splitlines()
    pp = pprint.PrettyPrinter(indent = 2)
    pp.pprint(olines)
    codes, defgateway, routes = '','',''
    i = 0
    for line in olines:
        if line.strip() == '':
            i += 1
            continue
        if i == 0:
            codes += line+'\n'
        if i == 1:
            defgateway += line+'\n'
        if i == 2:
            routes += line+'\n'
    print("---CODES---")
    print(codes)
    print("---GATEWAY---")
    print(defgateway)
    print("---ROUTES---")
    print(routes)
    i = 0
    route_dict = dict()
    route_types = {'S': 'Static',
                   'O': 'OSPF',
                   'C': 'Directly Connected',
                   'i': 'IS-IS',
                   'R': 'RIP',
                   'D': 'EIGRP',
                   'B': 'BGP'}
    for line in routes.splitlines():
        if (line.startswith("L")):
            print("Skipping: %s" % line)
            continue
        elif ("subnetted" in line):
            d = re.match(r'^\s*\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}(?P<subnetprefix>/\d{1,2}) is (?:variably )?subnetted', line).groupdict()
            subnetprefix = d['subnetprefix']
        else:
            interface = line.split(', ')[-1]
            type = route_types[line[0]]
            nexthop = None
        try:
            if line.startswith("C"):
                d = re.match(r'^C\s*(?P<subnet>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/\d{1,2}) is directly connected', line).groupdict()
                subnet = d['subnet']
            elif 'via' in line:
                d = re.match(r'^\S*\s\S*?\s+(?P<subnetaddr>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})(?P<subnetprefix>/\d{1,2})? \[\d*/\d*\] via (?P<nexthop>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})',line).groupdict()
                if d['subnetprefix'] != None:
                    subnet = d['subnetaddr']+d['subnetprefix']
                else:
                    subnet = d['subnetaddr']+subnetprefix
                nexthop = d['nexthop']
            if not route_dict.has_key(interface):
                route_dict[interface] = set()
            (route_dict[interface]).add((type, subnet, nexthop))
        except:
            print("Error on line:\n%s" % line)
            raise
    # Converting the sets back to lists
    for key in route_dict.keys():
        route_dict[key] = list(route_dict[key])
    return route_dict
