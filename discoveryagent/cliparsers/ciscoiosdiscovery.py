"""
This module aims to create and update model objects for netmapcore from parsed information
extracted through the ciscoiosparsers module.

Tested against ciscoios versions: 5.2.4
"""

import ciscoiosparsers
import ciscoiostasks
from discoveryagent.cliparsers.genericdiscovery import GenericPostProcessor
from netmapcore.models import *

def discoverDevice(ip='0.0.0.0', username='admin', password='testpass', proto='SSH2', port=22):
    """
    :param ip:
    :param username:
    :param password:
    :param proto:
    :param port:
    :return:
    """

    #Set up Tasks
    ciscoiostasks.env['host'] = ip
    ciscoiostasks.env['username'] = username
    ciscoiostasks.env['password'] = password
    ciscoiostasks.env['ignorecache'] = True

    if ip=='0.0.0.0':
        print('Invalid IP')
        return 1

    # Find dievice which already exists with same mgt_ip
    try:
        print("Testing for device with mgt-ip: %s" % ip)
        devobj = Device.objects.get(mgtip__address=ip)
        print("Object Found")
    except:
        print("No existing object")
        devobj = Device()
        # Get or create MGT IP IPAddress entry
        ipobj = IPAddress.objects.get_or_create(address=ip, prefixlen=32)[0]
        ipobj.save()
        devobj.mgtip = ipobj

    try:
        # Find the Hostname
        output = ciscoiostasks.ciscoios_get_config(ciscoiostasks.env)
        devobj.hostname = ciscoiosparsers.get_hostname(output)
        print ("Hostname Detected: %s" % devobj.hostname)
        devobj.devstatus = 'mngd'
        devobj.save()

        # Get the Interface List
        output = ciscoiostasks.ciscoios_get_interfaces(ciscoiostasks.env)
        interface_dict = ciscoiosparsers.parse_interface_config(output)

        # Update or create interface objects and directly connected networks+IP Addresses
        for intname in interface_dict.keys():
            if interface_dict[intname].has_key('ipv4'):
                ipobj = IPAddress.objects.get_or_create(address=interface_dict[intname]['ipv4'], prefixlen=32)[0]
                intobj = Interface.objects.get_or_create(device=devobj,
                                                         primary_ip=ipobj)[0]
                network = ipaddress.ip_network(unicode(interface_dict[intname]['ipv4']+'/'+interface_dict[intname]['netmask']), strict=False)
                subnetobj = NetworkSubnet.objects.get_or_create(address=unicode(network.network_address), prefixlen=network.prefixlen)[0]
                subnetobj.connected_to_known_device = True
                intobj.macaddr = interface_dict[intname]['macaddr']
                print("Saving interface object %s" % intobj)
                intobj.name = intname
                intobj.primary_ip = ipobj
                intobj.subnet = subnetobj
                ipobj.save()
                subnetobj.save()
                intobj.save()

        # Detect Routes
        output = ciscoiostasks.ciscoios_show_ip_route(ciscoiostasks.env)
        route_dict = ciscoiosparsers.parse_routes(output)
        for interface in route_dict.keys():
            for route in route_dict[interface]:
                # Ignore default routes
                if route[1] != '0.0.0.0/0':
                    type = route[0]
                    subnet = route[1]
                    nexthop = route[2]
                    # Create NetworkSubnet if it doesn't exist
                    network = ipaddress.ip_network(unicode(subnet))
                    subnetobj = NetworkSubnet.objects.get_or_create(address=unicode(network.network_address), prefixlen=network.prefixlen)[0]
                    if type == 'Directly Connected':
                        subnetobj.connected_to_known_device = True
                    else:
                        subnetobj.connected_to_known_device = False
                    subnetobj.save()
                    # Create IPObj for nexthop if it doesn't exist
                    routeobj = Route.objects.get_or_create(network=subnetobj, device=devobj)[0]
                    routeobj.subnet = subnetobj
                    routeobj.type = type
                    routeobj.device = devobj
                    routeobj.interface = Interface.objects.get(device=devobj, name=interface)
                    if nexthop != None:
                        nexthop_ipobj = IPAddress.objects.get_or_create(address=nexthop, prefixlen=32)[0]
                        nexthop_ipobj.save()
                        routeobj.nexthop = nexthop_ipobj
                    routeobj.save()


        # initiate post-processing tasks:
        g = GenericPostProcessor()
        orphanroutes = g.get_orphan_nexthops(devobj)
        g.create_discovered_devices(orphanroutes)
        g.cleanup_discovered_devices(devobj)

        """
        if settings.DEBUG:
            import pprint
            pp = pprint.PrettyPrinter(indent = 2)
            pp.pprint(interface_dict)
        """
    except:
        print ciscoiostasks.env['host']
        print ("Output String: ")
        print (output)
        print ("Discovery process failed. Make sure you are having the correct device type assigned to the " +
               "Credential Object.")
        print("---Traceback---")
        raise