from discoveryagent.models import CommandCache
from netmapcore.models import Device

def add_to_cmdcache(mgtip=None, cmd=None, output=None):
    #Do not cache if Device is not known
    try:
        devobj = Device.objects.get(mgtip__address=mgtip)
    except:
        print "Device not known - Skipping command caching"
        return
    cacheobj = CommandCache()
    cacheobj.device = devobj
    cacheobj.command = cmd
    cacheobj.output = output
    print("Saving cmd output to cache")
    cacheobj.save()


