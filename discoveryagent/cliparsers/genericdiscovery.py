"""
This module hosts the generic discovery tasks such as initiating first-time discovery of new devices manually and
wrappers for non-vendor specific tasks which may call vendor specific parsing tasks after discovering the device type
and vendor.
"""

from netmapcore.models import Device, Route, Interface

class GenericDiscoveryAgent():
    def device_initial_discovery(self, ip, username, password, port=22, proto='ssh'):
        """

        :param ip: IP Address of device to discover
        :param username: Admin username
        :param password: Admin password
        :param port: Management Port
        :param proto: protocol
        :return: 
        """
        return

    def create_new_device(self):
        return


class GenericPostProcessor():


    """
    This class does post-processing tasks as a follow-on to discovery processes. Routes are examined for next-hops to create
    "discovered" routers for next-hop addresses which are not the interface IP addresses for any known L3 device
    """
    def get_orphan_nexthops(self, deviceobj):
        """

        :param deviceobj:
        :return: Set of nexthop IPs which don't have an existing Device/Interface object referring to them.
        """
        routeqs = deviceobj.get_routes().exclude(nexthop=None)
        nexthopset = set()
        interfaceqs = Interface.objects.all()
        for r in routeqs:
            match_interfaces = interfaceqs.filter(primary_ip=r.nexthop)
            if len(match_interfaces)==0:
                nexthopset.add((r.nexthop, r.interface.subnet))
        return nexthopset

    def create_discovered_devices(self, nexthopset):
        # For each item in the orphan next-hop set, create a device with the hostname of that next-hop with
        # only 1 unknown interface with the IP of the next-hop
        for nst in nexthopset:
            # Create a device for each orphan next-hop in the set
            d, status = Device.objects.get_or_create(mgtip=nst[0])
            if status == True:
                d.hostname = "UR(%s)" % nst[0].address
                d.dev_comment = "Autodiscovered device based on route entry pointint to %s" % nst[0].address
                d.save()
            i, status = Interface.objects.get_or_create(device=d, primary_ip=nst[0])
            if status == True:
                i.device = d
                i.primary_ip = nst[0]
                i.subnet = nst[1]
                i.name = "unknown-int-ip-%s" % nst[0].address
                i.save()
            # Create the interface connecting to the network pointed to by the route
            routeqs = Route.objects.filter(nexthop=nst[0])
            for route in routeqs:
                i, status = Interface.objects.get_or_create(device=d, subnet=route.network, primary_ip=None)
                if status == True:
                    i.name = "unknown-int-net-%s" % route.network
                    i.save()

    def cleanup_discovered_devices(self, deviceobj):
        """
        Remove any discovered devices which have a similar interface to the managed device which has been processed.
        :param deviceobj:
        :return:
        """
        deletable_interfaces = Interface.objects.filter(device=deviceobj, name__startswith="unknown-int")
        for i in deletable_interfaces:
            i.delete()
        interfaceqs = Interface.objects.filter(device=deviceobj)
        print("Interfaces:")
        deletable_devices = set()
        for interface in interfaceqs:
            print("%s - %s - %s " % (
                interface.name,
                interface.primary_ip,
                interface.device
            ))
            discovered_int_qs = Interface.objects.filter(primary_ip=interface.primary_ip,
                                                         device__devstatus='dscv').exclude(device=deviceobj)
            for dscint in  discovered_int_qs:
                print("Redundant interface %s (%s) found on discovered device: %s" % (
                    dscint.name,
                    dscint.primary_ip,
                    dscint.device
                ))
                deletable_devices.add(dscint.device)

        for device in deletable_devices:
            print("deleting ")
            device.delete()