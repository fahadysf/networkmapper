from discoveryagent.cliparsers.cmdcacheops import add_to_cmdcache
from discoveryagent.models import CommandCache
from pexpectops import runcmd

env = {
        'host': '',
        'username': '',
        'password': '',
        'proto': 'SSH2',
        'port': 22,
        'ignorecache': True    }

# Testing Task
def fortios_check_device_status(env):
    cmd = "get system status"
    if  env['ignorecache']:
        cached_output = None
    else:
        cached_output = CommandCache.objects.filter(command=cmd).order_by('-tstamp')
    if cached_output==None or len(cached_output) == 0:
        outputstr = runcmd(env['host'], cmd)
        add_to_cmdcache(cmd=cmd, output=outputstr, mgtip=env['host'])
    else:
        outputstr = (cached_output[0]).output
    return outputstr

def fortios_get_routes(env):
    cmd = "get router info routing-table all"
    if  env['ignorecache']:
        cached_output = None
    else:
        cached_output = CommandCache.objects.filter(command=cmd).order_by('-tstamp')
    if cached_output==None or len(cached_output) == 0:
        print("Cached output not found")
        outputstr = runcmd(env, cmd)
        add_to_cmdcache(cmd=cmd, output=outputstr, mgtip=env['host'])
    else:
        print("Cached output found")
        outputstr = (cached_output[0]).output
    return outputstr

def fortios_get_interfaces(env):
    cmd = "get system interface"
    if  env['ignorecache']:
        cached_output = None
    else:
        cached_output = CommandCache.objects.filter(command=cmd).order_by('-tstamp')
    if cached_output==None or len(cached_output) == 0:
        print("Cached output not found")
        get_sys_int_output = runcmd(env, cmd)
        add_to_cmdcache(cmd=cmd, output=get_sys_int_output, mgtip=env['host'])
    else:
        print("Cached output found")
        get_sys_int_output = (cached_output[0]).output
    cmd = "show system interface"
    if  env['ignorecache']:
        cached_output = None
    else:
        cached_output = CommandCache.objects.filter(command=cmd).order_by('-tstamp')
    if cached_output==None or len(cached_output) == 0:
        print("Cached output not found")
        show_sys_int_output = runcmd(env, cmd)
        add_to_cmdcache(cmd=cmd, output=show_sys_int_output, mgtip=env['host'])
    else:
        print("Cached output found")
        show_sys_int_output = (cached_output[0]).output
    return ( get_sys_int_output, show_sys_int_output )

def fortios_get_config(env):
    cmd = "show"
    if  env['ignorecache']:
        cached_output = None
    else:
        cached_output = CommandCache.objects.filter(command=cmd).order_by('-tstamp')
    if cached_output==None or len(cached_output) == 0:
        print("Cached output not found")
        outputstr = runcmd(env, cmd)
        add_to_cmdcache(cmd=cmd, output=outputstr, mgtip=env['host'])
    else:
        print cached_output
        print("Cached output found")
        outputstr = (cached_output[0]).output
    return outputstr

def fortios_get_config_sys_global(env):
    cmd = "show system global"
    if  env['ignorecache']:
        cached_output = None
    else:
        cached_output = CommandCache.objects.filter(command=cmd).order_by('-tstamp')
    if cached_output==None or len(cached_output) == 0:
        print("Cached output not found or ignored")
        outputstr = runcmd(env, "show system global" )
        add_to_cmdcache(cmd=cmd, output=outputstr, mgtip=env['host'])
    else:
        print cached_output
        print("Cached output found")
        outputstr = (cached_output[0]).output
    return outputstr

def fortios_get_mac_address(port, env):
    cmd = "diagnose hardware deviceinfo nic %s | grep '^H[Ww][Aa]ddr'" % port
    if  env['ignorecache']:
        cached_output = None
    else:
        cached_output = CommandCache.objects.filter(command=cmd).order_by('-tstamp')
    if cached_output==None or len(cached_output) == 0:
        print("Cached output not found")
        outputstr = runcmd(env, cmd)
        add_to_cmdcache(cmd=cmd, output=outputstr, mgtip=env['host'])
    else:
        print cached_output
        print("Cached output found")
        outputstr = (cached_output[0]).output
    return outputstr