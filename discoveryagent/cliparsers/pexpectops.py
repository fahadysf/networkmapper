#!/usr/bin/env python
import re

import pexpect


def ssh_runcmd(cmd, host='', username='', password='', port=22):
    ssh_newkey = 'Are you sure you want to continue connecting'
    # my ssh command line
    p=pexpect.spawn('ssh -p %d %s@%s %s' % (port, username, host, cmd))
    i=p.expect([ssh_newkey,'(P|p)assword:',pexpect.EOF])
    if i==0:
        p.sendline('yes')
        i=p.expect([ssh_newkey,'(P|p)assword:',pexpect.EOF])
    if i==1:
        p.sendline(password)
        p.expect(pexpect.EOF)
    output = p.before
    output = re.sub(r'(Connection .+?closed by remote host.+$)', '', output)
    return output # print out the result

def runcmd(env, cmd):
    try:
        if env['proto'] == 'SSH2':
            return ssh_runcmd(
                cmd,
                host=env['host'],
                username=env['username'],
                password=env['password'],
                port=env['port'],
            )
        else:
            print env
    except:
        raise
