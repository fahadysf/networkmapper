from discoveryagent.cliparsers.cmdcacheops import add_to_cmdcache
from discoveryagent.models import CommandCache
from pexpectops import runcmd

env = {
        'host': '',
        'username': '',
        'password': '',
        'proto': 'SSH2',
        'port': 22,
        'ignorecache': True
    }

# Testing Task
def ciscoios_show_version(env):
    cmd = "show version"
    if  env['ignorecache']:
        cached_output = None
    else:
        cached_output = CommandCache.objects.filter(command=cmd).order_by('-tstamp')
    if cached_output==None or len(cached_output) == 0:
        outputstr = runcmd(env['host'], cmd)
        add_to_cmdcache(cmd=cmd, output=outputstr, mgtip=env['host'])
    else:
        outputstr = (cached_output[0]).output
    return outputstr

def ciscoios_show_ip_route(env):
    cmd = "show ip route"
    if  env['ignorecache']:
        cached_output = None
    else:
        cached_output = CommandCache.objects.filter(command=cmd).order_by('-tstamp')
    if cached_output==None or len(cached_output) == 0:
        print("Cached output not found")
        outputstr = runcmd(env, cmd)
        add_to_cmdcache(cmd=cmd, output=outputstr, mgtip=env['host'])
    else:

        print("Cached output found")
        outputstr = (cached_output[0]).output
    return outputstr

def ciscoios_get_interfaces(env):
    output = runcmd(env, "show interfaces")
    show_interfaces_output = ''
    for line in output.splitlines():
        if not (line.startswith("Connection to") and line.endswith("closed by remote host.")):
            show_interfaces_output += line+'\n'
    return show_interfaces_output

def ciscoios_get_config(env):
    cmd = "show running-config"
    if  env['ignorecache']:
        cached_output = None
    else:
        cached_output = CommandCache.objects.filter(command=cmd).order_by('-tstamp')
    if cached_output==None or len(cached_output) == 0:
        print("Cached output not found")
        outputstr = runcmd(env, cmd)
        add_to_cmdcache(cmd=cmd, output=outputstr, mgtip=env['host'])
    else:
        print cached_output
        print("Cached output found")
        outputstr = (cached_output[0]).output
    return outputstr


