#!/usr/bin/env python

#Setting up the django base directory and environment;
import os
import sys

proj_path  = os.path.abspath(os.path.dirname(__name__))
# This is so Django knows where to find stuff.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "networkmapper.settings")
sys.path.append(proj_path)
# This is so my local_settings.py gets loaded.
os.chdir(proj_path)

# This is so models get loaded.
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()


from discoveryagent.cliparsers import fortiosdiscovery, ciscoiosdiscovery
from netmapcore.models import Credential, IPAddress
import argparse

# This package is a CLI package to initiate Network Discovery

def create_credentials(*args):
    args = args[0]
    mgtip = args['mgtip']
    username = args['username']
    if args['protocol']==None:
        protocol='SSH2'
    else:
        protocol=args['protocol']
    if args['devtype'] == None:
        print("Please specify Device Type using --devtype.")
        print("Currently supported Device Types are:")
        print("#- devtype\t- Description")
        for i,type_tuple in enumerate(Credential.SUPPORTED_DEVICES):
            if type_tuple[0] != "unknown":
                print("%d- %s\t- %s" % (i, type_tuple[0], type_tuple[1]))
        return
    else:
        devtype = args['devtype']
    if args['port'] == None:
        port = 22
    else:
        port = args['port']
    import getpass
    print("Please enter the password for username '%s'" % username)
    password = getpass.getpass()
    print("Creating credential object: \n")
    try:
        ipaddr, status = IPAddress.objects.get_or_create(address=mgtip, prefixlen=32)
        cobj,status = Credential.objects.get_or_create(mgtip=ipaddr,username=username,
                                                proto=protocol, port=port)
        if status==False:
            print("Existing credential found for %s@%s \nUpdating password for existing credential" % (username, mgtip))
        cobj.password = password
        cobj.devtype = devtype
        cobj.save()
        print("Credential object created.")
        return cobj
    except:
        raise

def discover_device(*args):
    seed_ip = args[0]['seed']
    try:
        seed_credential_obj = Credential.objects.get(mgtip__address=seed_ip)
    except:
        print("Credential not found. Please ensure that a credential object for the seed IP exists.")
        return
    print("Starting discovery for %s with username: %s" % (seed_ip, seed_credential_obj.username))
    if seed_credential_obj.devtype=='fortios':
        fortiosdiscovery.discoverDevice(ip=seed_ip, username=seed_credential_obj.username, password=seed_credential_obj.password)
    elif seed_credential_obj.devtype=='ciscoios':
        ciscoiosdiscovery.discoverDevice(ip=seed_ip, username=seed_credential_obj.username, password=seed_credential_obj.password)
    else:
        print("Please edit credential and specify device type.")
    return

parser = argparse.ArgumentParser(description="""This is a CLI uility to initiate network discovery using networkmapper.
""")

subparsers = parser.add_subparsers(title="Commands",
                                   description="List of possible Operations for this commandline utility")

# Specify arguments for "create-credential" operation

parser_create_credential = subparsers.add_parser('create-credential', help="Create a credential")

parser_create_credential.add_argument('-m', '--mgtip', help="Management IP of Device", required=True)
parser_create_credential.add_argument('-u', '--username', help="Login Username", required=True)
parser_create_credential.add_argument('--protocol', nargs='?', help="Protocol (Optional)")
parser_create_credential.add_argument('--devtype', nargs='?', help="Device Type (Optional)")
parser_create_credential.add_argument('--port', nargs='?', help="Port (Optional)")

parser_create_credential.set_defaults(func=create_credentials)

# Specify arguments for "create-credential" operation

parser_discovery = subparsers.add_parser('discover', help="Initiate network discovery with specified seed device")

parser_discovery.add_argument('-s', '--seed', help="Management IP of Initial (Seed) Device", required=True)

parser_discovery.set_defaults(func=discover_device)


if __name__=="__main__":
    args =  parser.parse_args()
    args.func(vars(args))
