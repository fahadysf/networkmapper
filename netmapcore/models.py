import datetime
import uuid

from django.db import models
# To work with IP Addresses
import ipaddress


class IPAddress(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
    TYPE_CHOICES = (
        ('v4', "IPv4"),
        ('v6', "IPv6"),
    )
    type = models.CharField(max_length=2, choices=TYPE_CHOICES, default='v4')
    address = models.GenericIPAddressField(
        protocol='both', unpack_ipv4=True)
    prefixlen = models.IntegerField(default=32)
    ip_comment = models.TextField(max_length=1024, blank=True, null=True, default=None)

    @classmethod
    def create(cls, address, prefixlen):
        """

        :param address: IP Address (string) e.g. "192.168.0.10"
        :param prefixlen: Prefix Length (int) e.g. 24

        :return: Returns an IPAddress model instance object with
                 attributes set according to the arguments.
        """
        ip = cls(address=address, prefixlen=prefixlen)
        return ip

    def cidr(self):
        return unicode(str(self.address) + '/' + str(self.prefixlen))

    def network(self):
        return ipaddress.ip_network(self.cidr(), strict=False)

    def ipstr(self):
        return str(self.address)

    def __str__(self):
        return self.cidr()


class Device(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
    TYPE_CHOICES = (
        ( 'mngd', 'managed'),
        ( 'dscv', 'discovered'),
    )
    hostname = models.CharField("hostname", max_length=512)
    fqdn = models.CharField("fqdn", max_length=1024, null=True)
    vendor = models.CharField("vendor", max_length=20, null=True)
    mgtip = models.ForeignKey(IPAddress, verbose_name="management IP", null=True, on_delete=models.PROTECT, related_name='device_mgtip')
    dev_comment = models.TextField("comment", max_length=1024, blank=True, null=True, default=None)
    devstatus = models.CharField("status", max_length=4, choices=TYPE_CHOICES, default='dscv')
    last_updated = models.DateTimeField("last updated", default=datetime.datetime.now, null=True)

    @classmethod
    def create(cls, hostname, mgtip):
        """
        Creates a Device model instance.

        :param hostname: Hostname (string) e.g. "router-1"
        :param mgtip: IPAddress model instance.

        :return: Returns a Device model instance.
        """
        device = cls(hostname=hostname, mgtip_=mgtip)
        return device

    def get_interfaces(self):
        intqs = Interface.objects.filter(device=self)
        return intqs

    def get_routes(self):
        routeqs = Route.objects.filter(device=self)
        return routeqs

    def __str__(self):
        if self.fqdn != None:
            return self.fqdn
        else:
            return self.hostname



"""
TODO: Work needs to be done on the NetworkSubnet Class to ensure that the smallest subnet can always be detected out of
many NetworkSubnet objects
"""

class NetworkSubnet(IPAddress):
    connected_to_known_device = models.BooleanField(default=False)
    subnet_comment = models.TextField(max_length=1024, blank=True, null=True, default=None)

    @classmethod
    def create(cls, address, prefixlen):
        """

        :param address: IP Address (string) e.g. "192.168.0.10"
        :param prefixlen: Prefix Length (int) e.g. 24

        :return: Returns an NetworkSubnet model instance object with
                 attributes set according to the arguments.
        """
        subnet = cls(address=address, prefixlen=prefixlen)
        # From given Address and Prefix, extract the Network Address and
        # set it as the address.
        subnet.address = str(ipaddress.ip_network(
            address + '/' + str(prefixlen), strict=False).network_address)
        return subnet

    def save(self, **kwargs):
        """
        Custom save() method which ensures that self.address is actually
        a network address.
        """
        try:
            ipaddress.ip_network(self.cidr(), strict=True)
        except:
            raise

        super(NetworkSubnet, self).save()


class Interface(models.Model):
    TYPE_CHOICES = (
        ( 'vlan', 'Switch Virtual Interface (VLAN Interface)'),
        ( 'l3int', 'Physical L3 Interface'),
    )
    id = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
    name = models.CharField(max_length=256)
    primary_ip = models.ForeignKey(IPAddress, related_name='interface_ip', on_delete=models.PROTECT, null=True)
    subnet = models.ForeignKey(NetworkSubnet, related_name='interface_subnet', on_delete=models.PROTECT, null=True)
    device = models.ForeignKey('Device', on_delete=models.CASCADE)
    intf_comment = models.TextField(max_length=1024, blank=True, null=True, default=None)
    type = models.CharField(max_length=5, choices=TYPE_CHOICES, default='l3int')
    macaddr = models.CharField(max_length=17, blank=True, null=True)
    last_updated = models.DateTimeField(auto_now=True)

    @classmethod
    def create(cls, name, device):
        """
        Creates an Interface model instance

        :param name: Interface l
        :param device:
        :return:
        """
        interface = cls(name=name, device=device)

    def network(self):
        return ipaddress.ip_network(str(self.primary_ip) + '/' + str(self.primary_ip_netmask))

    def __str__(self):
        if self.device.fqdn:
            return "Interface %s on device %s" % \
                   (self.name, self.device.fqdn)
        else:
            return "Interface %s on device %s" % \
                   (self.name, self.device.hostname)


class InterfaceIPAddress(IPAddress):
    intf = models.ForeignKey(
        Interface,
        null=True,
        on_delete=models.CASCADE,
        related_name='ipaddress_interface'
    )
    parent_subnet = models.ForeignKey(
        NetworkSubnet,
        null=True,
        on_delete=models.PROTECT
    )

    @classmethod
    def create(cls, address, prefixlen, interface):
        """
        Creates an InterfaceIPAddress object.

        :param address: IP Address (string) e.g. "192.168.0.10"
        :param prefixlen: Prefix Length (int) e.g. 24
        :param interface: Interface model instance (netmapcore.models.Interface)

        :return: Returns an InterfaceIPAddress model instance object with
                 attributes set according to the arguments.
        """
        subnet = cls(address=address, prefixlen=prefixlen, interface=interface)
        subnet.address = ipaddress.ip_network(self.cidr(), strict=False)
        return subnet

    def save(self, **kwargs):
        """
        Custom save() method which creates and saves corresponding
        NetworkSubnet model instance object if it doesn't already
        exit.
        """
        netaddr = str(ipaddress.ip_network(
            self.cidr(), strict=False).network_address)
        parent_subnet_res = NetworkSubnet.objects.filter(
            address=netaddr, prefixlen=self.prefixlen)
        if len(parent_subnet_res):
            print("NetworkSubnet Object already exists")
            self.parent_subnet = parent_subnet_res[0]
        else:
            n = NetworkSubnet()
            n.address = str(ipaddress.ip_network(
                self.cidr(), strict=False).network_address)
            n.prefixlen = self.prefixlen
            n.save()
            self.parent_subnet = n
            super(InterfaceIPAddress, self).save()


class NextHopIP(IPAddress):
    known_device = models.BooleanField(default=False)

    def save(self, **kwargs):
        """
        Custom save() method which populates the known_device flag
        if the IP is already known to be assigned to an Interface
        """
        if len(InterfaceIPAddress.objects.filter(address=self.address, prefixlen=self.prefixlen)):
            self.known_device = True
        super(NextHopIP, self).save()


class Route(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid1)
    device = models.ForeignKey(
        'Device', related_name='device', on_delete=models.CASCADE)
    network = models.ForeignKey(
        'NetworkSubnet', related_name='network', on_delete=models.PROTECT)
    nexthop = models.ForeignKey(
        'IPAddress', related_name='nexthop', on_delete=models.PROTECT, null=True)
    interface = models.ForeignKey(
        'Interface', related_name='route_interface', on_delete=models.CASCADE, null=True)
    type = models.CharField(max_length=20, default="")
    last_updated = models.DateTimeField(auto_now=True)


class Credential(models.Model):
    SUPPORTED_DEVICES = (
        ( 'unknown', 'Unknown'),
        ( 'fortios', 'Fortigate FortiOS v4.0+'),
        ( 'ciscoios', 'Cisco IOS 12+/15+'),
    )

    mgtip = models.ForeignKey('IPAddress', related_name='cred_mgtip', on_delete=models.PROTECT)
    proto = models.CharField(max_length=10, choices=(('SSH1', 'SSH1'),('SSH2', 'SSH2'), ('TELNET', 'TELNET')), default='SSH2')
    port = models.IntegerField(default=22)
    username = models.CharField(max_length=128, null=True)
    password = models.CharField(max_length=256, null=True)
    devtype = models.CharField(max_length=24, choices=SUPPORTED_DEVICES, default='unknown')

    def __str__(self):
        return "Account %s for IP %s" % (self.username, self.mgtip.address)