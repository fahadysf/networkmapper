from rest_framework import serializers, viewsets

from netmapcore.models import Device, IPAddress


# Serializers define the API representation.
class DeviceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Device
        fields = (
            'hostname',
            'fqdn',
            'vendor',
            'mgtip',
            'dev_comment',
            'devstatus',
            'last_updated',
        )

# ViewSets define the view behavior.
class DeviceViewSet(viewsets.ModelViewSet):
    queryset = Device.objects.all()
    serializer_class = DeviceSerializer

# Serializers define the API representation.
class IPAddressSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = IPAddress
        fields = (
            'type',
            'address',
            'prefixlen',
            'ip_comment',
        )

# ViewSets define the view behavior.
class IPAddressViewSet(viewsets.ModelViewSet):
    queryset = IPAddress.objects.all()
    serializer_class = IPAddressSerializer
