from django.test import TestCase
import sys

from netmapcore.models import NetworkSubnet, IPAddress, InterfaceIPAddress
from netmapcore.models import NextHopIP, Interface, Device

# Create your tests here.


class CreateModels(TestCase):

    def test_operations_IPAddress(self):
        print ("-> Cerating IPAddress Object")
        try:
            ip = IPAddress.create(address='192.0.2.10', prefixlen=24)
            ip.save()
            print("IPAddress created: %s" % ip)
        except:
            raise
        del(ip)

        print ("-> Listing All IPAddress Objects")
        try:
            for ip in IPAddress.objects.all():
                print("IPAddress: %s" % ip)
        except:
            raise

        print ("-> Searching IPAddress Object")
        try:
            ip = IPAddress.objects.get(address='192.0.2.10', prefixlen=24)
            print("IPAddress Found: %s" % ip)
        except:
            raise

        print ("-> Deleting IPAddress Object")
        try:
            ip.delete()
            print("IPAddress deleted: %s" % ip)
        except:
            raise
