from django import forms

from models import *


class CredentialForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = Credential
        fields = ( 'mgtip', 'username', 'password', 'proto', 'port', 'devtype')

class DeviceForm(forms.ModelForm):
    class Meta:
        model = Device
        fields = ( 'hostname', 'fqdn', 'vendor', 'mgtip', 'dev_comment', 'devstatus', 'last_updated')

class NetworkForm(forms.ModelForm):
    class Meta:
        model = NetworkSubnet
        fields = ( 'address', 'prefixlen', 'type', 'ip_comment'         )
