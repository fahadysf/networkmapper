from django.contrib import admin

from netmapcore.forms import CredentialForm
# Register your models here.
from netmapcore.models import *


class DeviceAdmin(admin.ModelAdmin):
    list_display = ( 'hostname','fqdn', 'mgtip', 'vendor', 'last_updated')
    pass

class IPAddressAdmin(admin.ModelAdmin):
    list_display = ('cidr', 'address', 'prefixlen', 'ip_comment')
    search_fields = ('address', )

class CredentialAdmin(admin.ModelAdmin):
    form = CredentialForm

class InterfaceAdmin(admin.ModelAdmin):
    def interface_ip(self, obj):
        if obj.primary_ip !=None:
            return ("%s" % obj.primary_ip.address)
        else:
            return "None"
    def subnet_mask(self, obj):
        return ("%s" % unicode(obj.subnet.ipaddress_ptr.network().netmask))
    list_display = ('name', 'device', 'interface_ip', 'subnet_mask', 'macaddr', 'last_updated')
    search_fields = ('device', 'name', 'primary_ip')

class NetworkSubnetAdmin(admin.ModelAdmin):
    list_display = ('cidr', 'address', 'prefixlen', 'connected_to_known_device')
    search_fields = ('address', )

class RouteAdmin(admin.ModelAdmin):
    def nexthop_ip(self, obj):
        if obj.nexthop != None:
            return ("%s" % obj.nexthop.address)
        else:
            return '-'

    def int_name(self, obj):
        return ("%s" % obj.interface.name)
    list_display = ('network', 'nexthop_ip', 'device', 'int_name', 'type', 'last_updated')
    search_fields = ('address', )

admin.site.register(Device, DeviceAdmin)
admin.site.register(IPAddress, IPAddressAdmin)
admin.site.register(Credential, CredentialAdmin)
admin.site.register(Interface, InterfaceAdmin)
admin.site.register(NetworkSubnet, NetworkSubnetAdmin)
admin.site.register(Route, RouteAdmin)
