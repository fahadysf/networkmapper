# Create your views here.

import json
import uuid

from django.core import serializers
from django.shortcuts import render

from  networkmapper.settings import STATIC_URL
from .models import Device, IPAddress, NetworkSubnet, Interface

GLOBALCONTEXT = {
    'appname': "Network Mapper",
}

def createShapeJSON(obj, x, y, type='device'):
    #Generate UUIDs for the composite shape components
    # Set UUID for the Shape composite to the UUID of the object being displayed
    composite_uuid = str(obj.id)
    rect_uuid = uuid.uuid1().__str__()
    bounding_uuid = uuid.uuid1().__str__()
    image_shape_dict = { "type": "draw2d.shape.basic.Image",
        "id": uuid.uuid1().__str__(),
        "x": x+152,
        "y": y+80,
        "width": 32,
        "height": 32,
        "userData": {},
        "stroke": 1,
        "alpha": 1,
        "composite": composite_uuid,
    }
    label_shape_dict = {
        "type": "draw2d.shape.basic.Text",
        "id": uuid.uuid1().__str__(),
        "x": x + 130,
        "y": y + 110,
        "height": 19,
        "alpha": 1,
        "userData": {},
        "color": "#1B1B1B",
        "stroke": 0,
        "outlineStroke": 0,
        "fontSize": 12,
        "fontColor": "#000000",
        "fontFamily": "Arial",
        "isBold": False,
        "isFront": True,
        "composite": composite_uuid
    }
    rectangle_shape_dict = {
        "type": "draw2d.shape.basic.Rectangle",
        "id": rect_uuid,
        "x": x+120,
        "y": y+76,
        "width": 100, "height": 60,
        "composite": composite_uuid, "cssClass": "draw2d_shape_basic_Rectangle",
        "stroke": 1, "alpha": 1, "radius": 10, "color": "#000000",
        "bgColor": "#ffffff",
        "userData": {},
        "ports": [],
        "objectType": type
    }
    bounding_rectangle_shape_dict = {
        "type": "draw2d.shape.basic.Rectangle",
        "id": bounding_uuid,
        "x": x+120,
        "y": y+76,
        "width": 100, "height": 60,
        "composite": composite_uuid, "cssClass": "draw2d_shape_basic_Rectangle",
        "stroke": 1, "alpha": 0,
        "userdata": dict()
    }
    if type == 'device':
        image_shape_dict["path"] = STATIC_URL + "img/icons/router-96.png"
        label_shape_dict["text"] = obj.hostname
        # Create ports based on number of interfaces or connected devices
        interface_set = Interface.objects.filter(device=obj)

        for interface in interface_set:
            rectangle_shape_dict["ports"].append({
            "type": "draw2d.HybridPort",
            "width": 6,
            "height": 6,
            "alpha": 1,
            "angle": 0,
            "userData": {
                "interface": "%s : %s" % (interface.device, interface.name),
                "network_id": str(interface.subnet.id)
            },
            "cssClass": "draw2d_HybridPort",
            "bgColor": "#4F6870",
            "color": "#1B1B1B",
            "stroke": 2,
            "dasharray": None,
            "maxFanOut": 20000,
            "name": str(interface.id),
            "port": "draw2d.HybridPort",
            "locator": "draw2d.layout.locator.OutputPortLocator"
            })
    elif type == 'network':
        image_shape_dict["path"] = STATIC_URL + "img/icons/cloud-512.png"
        label_shape_dict["text"] = obj.cidr()
        #Create ports based on number of interfaces or connected devices
        connections = Interface.objects.filter(subnet=obj)
        for c in connections:
            rectangle_shape_dict["ports"].append({
                "type": "draw2d.HybridPort",
                "width": 6,
                "height": 6,
                "alpha": 1,
                "angle": 0,
                "userData": { "network": "%s" % (c.subnet) },
                "cssClass": "draw2d_HybridPort",
                "bgColor": "#4F6870",
                "color": "#1B1B1B",
                "stroke": 2,
                "dasharray": None,
                "maxFanOut": 20000,
                "portType": "networkport",
                "name": "c2-%s" % str(c.id),
                "port": "draw2d.HybridPort",
                "locator": "draw2d.layout.locator.InputPortLocator"
            })
    composite_shape_dict = {
            "type": "draw2d.shape.composite.Group",
            "id": composite_uuid,
            "x": x + 120,
            "y": y + 76,
            "width": 102, "height": 62,
            "cssClass": "draw2d_shape_composite_Group",
            "stroke": 1,
            "alpha": 0,
            "isSelectable": False
        }
    composite_shape_dict["userData"] = {
        "name": label_shape_dict["text"],
        "id": str(obj.id),
        "type": type,
        "rect_obj": rect_uuid,
    }
    jsonlist = [
        composite_shape_dict,
        bounding_rectangle_shape_dict,
        rectangle_shape_dict,
        label_shape_dict,
        image_shape_dict,
    ]

    return json.dumps(jsonlist)

def devicelist(request):
    from netmapcore.forms import DeviceForm as deviceform
    device_list = Device.objects.all()
    dev_obj_forms = list()
    for dev in device_list:
        dev_obj_forms.append(deviceform(instance=dev))
    context = {
        'view_name': "Device List",
        'deviceform': deviceform,
        'device_list': device_list,
        'dev_obj_forms': dev_obj_forms,
        'discovered_device_list': device_list.filter(devstatus='dscv'),
        'managed_device_list': device_list.filter(devstatus='mngd'),
        'globalcontext': GLOBALCONTEXT,
    }
    return render(request, 'devicelist.html', context)

def networklist(request):
    from netmapcore.forms import NetworkForm as networkform
    network_list = NetworkSubnet.objects.all()
    net_obj_forms = list()
    for net in network_list:
        net_obj_forms.append(networkform(instance=net))
    context = {
        'view_name': "Network List",
        'networkform': networkform,
        'network_list': network_list,
        'net_obj_forms': net_obj_forms,
        'globalcontext': GLOBALCONTEXT,
    }
    return render(request, 'networklist.html', context)

def networkmap(request):
    device_list = Device.objects.all()
    interface_list = set()
    shapes = list()
    interface_jsobj_dict = dict()
    # Create serialized JSON Object definitions for Devices, Networks and Interfaces
    devices_json_list = serializers.serialize('json', device_list)
    network_list = set()
    network_qset = IPAddress.objects.filter(id=uuid.uuid1(None))
    for i, device in enumerate(device_list):
        interfaces = Interface.objects.filter(device=device)
        interface_jsobj_dict[str(device.id)] = json.loads(serializers.serialize('json', interfaces))
        # While processing devices populate the list of interfaces
        for interface in interfaces:
            interface_list.add(interface.subnet)
            q = IPAddress.objects.filter(id=interface.subnet.id)
            network_qset = network_qset | q
        network_list = json.loads(serializers.serialize('json', network_qset))
        shapes.append(createShapeJSON(device, i*200, 0, type='device'))
    for i, network in enumerate(interface_list):
        xpos = (i*200)%1000
        ypos = 200+int((i*200)/1000.0)*200
        shapes.append(createShapeJSON(network, xpos, ypos, type='network'))


    serialized_interfaces = json.dumps(interface_jsobj_dict)
    if len(network_list) != 0:
        serialized_networks =  json.dumps(network_list)
    else:
        serialized_networks = json.dumps({})

    context = {
        'view_name': "Network Map",
        'shapes': shapes,
        'globalcontext': GLOBALCONTEXT,
        'serialized_devices': devices_json_list,
        'serialized_interfaces': serialized_interfaces,
        'serialized_networks':  serialized_networks
    }

    return render(request, 'networkmap.html', context)

