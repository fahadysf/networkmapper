"""
networkmapper URL Configuration
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView
from rest_framework import routers

from netmapcore.serializers import DeviceViewSet, IPAddressViewSet
from netmapcore.views import devicelist, networklist, networkmap

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'devices', DeviceViewSet)
router.register(r'ipaddresses', IPAddressViewSet)

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^restapi/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^devices/', devicelist, name='devices' ),
    url(r'^networks/', networklist, name='networks' ),
    url(r'^networkmap/', networkmap, name='networkmap'),
    url(r'^.*$', RedirectView.as_view(pattern_name='devices', permanent=False)),
]
